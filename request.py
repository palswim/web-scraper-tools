import pycurl
import urllib.request
import http.cookiejar
from util import eprint

class RequesterBase(object):
	proxies = None
	def __init__(self, proxy):
		proxies = urllib.request.getproxies()
		if proxy is not None:
			proxies['http'] = proxy
			proxies['https'] = proxy
		self.proxies = proxies

		self.properties = Properties()
		self.abort = False
	def _attach_cookies(self, cookiefile):
		cookiejar = http.cookiejar.MozillaCookieJar(cookiefile)
		if cookiefile is None:
			self.cookiejar = None
		else:
			self.cookiejar = cookiejar
			try:
				cookiejar.load()
			except OSError:
				pass
		return cookiejar
	def get(self, url, properties = None, referrer = None):
		if self.abort:
			raise KeyboardInterrupt
		headers = {}
		def set_headers(props):
			if props is not None:
				for name, val in props.to_list():
					headers[name] = val
		set_headers(self.properties)
		set_headers(properties)
		return self._get(url.strip(), headers)
	def _get(self, url, headers): raise NotImplementedError
	def persist(self):
		if self.cookiejar:
			self.cookiejar.save(ignore_discard = True, ignore_expires = True)

class URLLibRequester(RequesterBase):
	def __init__(self, proxy, cookiefile = None):
		super().__init__(proxy)
		handler_proxies = urllib.request.ProxyHandler(self.proxies)
		self.opener = urllib.request.build_opener(handler_proxies
			, urllib.request.HTTPCookieProcessor(self._attach_cookies(cookiefile)))
	def _get(self, url, headers):
		req = urllib.request.Request(url, headers = headers)
		with self.opener.open(req) as response:
			return response.read()

try:
	from requests import Session
except ImportError:
	Session = False
if Session:
	class RequestsRequester(RequesterBase):
		proxies = None
		def __init__(self, proxy, cookiefile = None):
			super().__init__(proxy)
			self.session = Session()
			self.session.cookies = self._attach_cookies(cookiefile)
		def _get(self, url, headers):
			with self.session.get(url, headers = headers, proxies = self.proxies) as response:
				response.raise_for_status()
				return response.content
	Requester = RequestsRequester
else:
	Requester = URLLibRequester


class Properties(object): # HTTP Headers
	_prop_header_map = {
		'user_agent': 'User-Agent', 'connection': 'Connection',
		'accept': 'Accept', 'accept_lang': 'Accept-Language',
		'referrer': 'Referer',
	}
	def __init__(self):
		self.headers_addl = []
		for name in self._prop_header_map.keys():
			setattr(self, name, None)
	def add_addl(self, name, val):
		self.headers_addl.append((name, val))
	def to_list(self):
		return ([(header, getattr(self, name)) for name, header
				in self._prop_header_map.items() if (getattr(self, name) is not None)]
			+ [(name, val) for name, val in self.headers_addl if (val is not None)])

class Curl(object):
	def __init__(self, proxy, user_agent, cookiefile = None):
		self.proxy = proxy
		self.user_agent = user_agent
		self.cookiefile = cookiefile
		self.abort = False
	def get(self, url, referrer = None):
		if self.abort:
			raise KeyboardInterrupt
		c = pycurl.Curl()
		c.setopt(c.URL, url)
		c.setopt(c.FOLLOWLOCATION, 1)
		if self.proxy:
			c.setopt(c.PROXY, self.proxy)
		if self.user_agent:
			c.setopt(c.USERAGENT, self.user_agent)
		if self.cookiefile:
			c.setopt(c.COOKIEFILE, self.cookiefile)
			c.setopt(c.COOKIEJAR, self.cookiefile)
		if referrer:
			c.setopt(c.HTTPHEADER, ['Referer: ' + referrer])
		data = b''
		def write_data(buf):
			nonlocal data
			data += buf
		c.setopt(c.WRITEFUNCTION, write_data)
		try:
			c.perform()
		except pycurl.error as ex:
			eprint('Error: {}', ex)
			return None
		c.close()
		return data
	def persist(self):
		pass

