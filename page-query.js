function getChain(qContent, attrContent, qNext, attrNext, qTitle) {
	var items = [];
	var queryDoc = function (dom) {
		var elsContent = dom.querySelectorAll(qContent);
		var vals = [];
		var getElementData = function (el, attr) { return el[attr || "innerText"]; }
		for (var i = 0; i < elsContent.length; i++)
			vals.push(getElementData(elsContent[i], (attrContent || "innerHTML")));
		var urlNext = getElementData(dom.querySelector(qNext), attrNext);
		if ((urlNext == "#") || (urlNext == (document.location.href + "#")))
			urlNext = null;
		var title = getElementData(dom.querySelector(qTitle));
		return {vals: vals, next: urlNext, title: title};
	}
	var processDoc = function (dom, url) {
		var item = queryDoc(dom);
		item.url = url;
		items.push(item);
		if (item.next)
			getFromURL(item.next);
		else
			console.log(items);
	}

	var getFromURL = function (url) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", url);
		xhr.onreadystatechange = function () {
			if (xhr.readyState == XMLHttpRequest.DONE) {
				if ((xhr.status >= 200) && (xhr.status < 400)) {
					var dom = new DOMParser().parseFromString(xhr.responseText, "text/html");
					processDoc(dom, url);
				} else if ((xhr.status > 400) && processFail) {
					alert("HTTP " + xhr.status + ": \n" + url);
					console.log(items);
				}
			}
		};
		xhr.onerror = function () {
			alert("Error: " + url);
			console.log(items);
		};
		xhr.send(null);
	}

	processDoc(document, document.location.href);
}

