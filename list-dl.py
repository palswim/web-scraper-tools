import sys
import signal
import os
import argparse
import json
import jmespath
import urllib.parse
import urllib.error
from request import Requester, Properties as RequestProperties
from util import eprint

def split_index(s_idx):
	if s_idx is None:
		return (0, 0)
	else:
		s = s_idx.split('.')
		return (int(s[0]), (int(s[1]) if (len(s) > 1) else 0))

def __run(l, expression, referer_exp, proxy, user_agent, outdir, start, jump, add_extension, skip_exist, cookiefile, verbose):
	req = Requester(proxy, cookiefile)
	req.properties.user_agent = user_agent

	start_idx, start_page = split_index(start)
	jump_idx, jump_page = split_index(jump)
	exp_items = jmespath.compile(expression)
	exp_referer = (None if (referer_exp is None) else jmespath.compile(referer_exp))

	for i, d in enumerate(l):
		if i < jump_idx:
			continue
		dir_out = os.path.join(outdir, str(i + start_idx))
		os.makedirs(dir_out, exist_ok=True)
		files_nowrite = (set(os.listdir(dir_out)) if skip_exist else ())
		for j, url in enumerate(exp_items.search(d)):
			if (i == jump_idx) and (j < jump_page):
				continue

			name = str(j + start_page)
			if add_extension:
				_, ext = os.path.splitext(u.path)
				name += ext
			if name in files_nowrite:
				continue

			try:
				props = RequestProperties()
				props.referrer = (None if (exp_referer is None) else exp_referer.search(d))
				data = req.get(url, props)
			except OSError as ex:
				eprint('{}\n\t{}', url, ex)
				return False
			u = urllib.parse.urlparse(url)
			path_file = os.path.join(dir_out, name)
			with open(path_file, 'wb') as f_out:
				f_out.write(data)
			if verbose:
				eprint('Wrote file: {} ({})', path_file, len(data))
	return True

def __main():
	parser = argparse.ArgumentParser(description="""Download a list of URLs""")
	parser.add_argument(dest='infile', nargs='?', help="""Read list of URLs from FILE (or "-" for STDIN)""", metavar='FILE')
	parser.add_argument('-e', '--expression', dest='exp', default='vals'
		, help="""Get list of URLs from JMESPath (JSONPath) EXPRESSION (default to "vals")""", metavar='EXPRESSION')
	parser.add_argument('-r', '--referer', '--referrer', dest='referer_exp', default='url'
		, help="""Set Referer (HTTP Header) from JMESPath (JSONPath) EXPRESSION (default to "url")""", metavar='EXPRESSION')
	parser.add_argument('-p', '--proxy', dest='proxy', help="""Download over Proxy URI (e.g. socks5://localhost:9050)""", metavar='URI')
	parser.add_argument('-s', '--start', dest='start', help="""Start file numbering at INDEX (default to 0.0)""", metavar='INDEX')
	parser.add_argument('-j', '--jump', dest='jump', help="""Start downloads at 0-based INDEX in item list (e.g., "2", "3.1", default to "0.0")""", metavar='INDEX')
	parser.add_argument('-X', '--omit-extension', dest='omit_extension', action='store_true', help="""Do not add extension to files""")
	parser.add_argument('-k', '--skip-exist', dest='skip_exist', action='store_true', help="""Skip (do not overwrite) existing files""")
	parser.add_argument('-c', '--cookiefile', dest='cookiefile', help="""Use FILE for Cookies (read/write)""")
	parser.add_argument('-u', '--user-agent', dest='user_agent', help="""Set User Agent to STRING""", metavar='STRING')
	parser.add_argument('-o', '--outdir', dest='outdir', default=None, help="""Output to DIRECTORY instead of current""", metavar='DIRECTORY')
	parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help="""Output more messages""")
	args = parser.parse_args()

	signal.signal(signal.SIGINT, lambda s, f: sys.exit(0))
	with (sys.stdin if (args.infile is None) else open(args.infile, 'r')) as f_in:
		l = json.loads(f_in.read())
	try:
		result = __run(([l] if isinstance(l, dict) else l)
			, args.exp, args.referer_exp
			, args.proxy, args.user_agent, (os.getcwd() if args.outdir is None else args.outdir)
			, args.start, args.jump, (not args.omit_extension), args.skip_exist
			, args.cookiefile, args.verbose)
	except KeyboardInterrupt:
		result = False
	if not sys.flags.interactive:
		sys.exit(0 if result else 1)

if __name__ == '__main__':
	__main()

