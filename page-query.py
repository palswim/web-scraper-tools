import sys
import argparse
import json
import signal
import urllib.parse
import urllib.error
from bs4 import BeautifulSoup
from request import Curl as CurlWrapper, Requester
from util import eprint

class DataSelector(object):
	def __init__(self, selector, attrib, one = False):
		self.selector = selector
		self.attrib = attrib
		self.one = one
	def select(self, soup):
		if self.one:
			el = soup.select_one(self.selector)
			return self.get_value(el) if el else None
		els = soup.select(self.selector)
		return [v for v in (self.get_value(el) for el in els) if v is not None]
	def get_value(self, element):
		if self.attrib is None:
			return ''.join(str(c) for c in element.children)
		else:
			try:
				return element[self.attrib].strip()
			except KeyError:
				return None
class SelectAll(object):
	def select(self, soup):
		return str(soup)

def get_data(url, req, process_data):
	while url:
		try:
			data = req.get(url)
		except OSError as ex:
			eprint('{}\n\t{}', url, ex)
			break
		except KeyboardInterrupt:
			break
		if data:
			vals, url_next = process_data(url, data)
			yield vals
			if not url_next or url_next == '#':
				break
			url = urllib.parse.urljoin(url, url_next)
def _get_stdin(url, req, process_data):
	vals, url = process_data(url, sys.stdin.buffer.read())
	return [vals]

def query_data(data, *selectors):
	b = BeautifulSoup(data, features='lxml', multi_valued_attributes=None)
	return (b, *((s.select(b) if s else None) for s in selectors))

def __run(get_f_out, url, input_list, proxy, user_agent
	, selector_data, attrib_data
	, selector_next, attrib_next
	, selector_title, attrib_title
	, cookiefile, verbose = False
):
	sel_data = DataSelector(selector_data, attrib_data) if selector_data else None
	sel_next = DataSelector(selector_next, attrib_next, True) if selector_next else None
	sel_title = DataSelector(selector_title, attrib_title, True) if selector_title else None
	req = Requester(proxy, cookiefile)
	req.properties.user_agent = user_agent
	req.properties.accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8'
	req.properties.accept_lang = 'en-US,en;q=0.5'
	def flag_abort(*p): req.abort = True
	signal.signal(signal.SIGINT, flag_abort)

	def process_data(url, data):
		b, vals, url_next, title = query_data(data, sel_data, sel_next, sel_title)
		if verbose:
			eprint('Elements: {}', len(vals))
			eprint('Next: {}', (url_next or 'N/A'))
		d = {'url': url, 'vals': vals}
		if title is not None:
			d['title'] = title
		return d, url_next
	def data_all(url, data):
		b, url_next = query_data(data, sel_next)
		return data, url_next

	if input_list:
		def get(url, *p):
			with (sys.stdin if url is None else open(url, 'r')) as f_in:
				while True:
					u = f_in.readline()
					if not u:
						break
					u = u.strip()
					if u:
						yield get_data(u, *p)
	else:
		get = (_get_stdin if (url is None) else get_data)

	count = 0
	with get_f_out() as f_out:
		if sel_data:
			sets = list(get(url, req, process_data))
			f_out.write(json.dumps((sets if (sel_next or (len(sets) != 1)) else sets[0])
				, indent='\t').encode('utf-8'))
			count = sum(len(s['vals']) for s in sets)
		else:
			for p in get(url, req, data_all):
				f_out.write(p)
				count += 1
	req.persist()
	return req, count

def __main():
	parser = argparse.ArgumentParser(description="""Gather information from a series of pages/URLs""")
	parser.add_argument(dest='url', help="""Start with page at URL (or "-" for STDIN)""", metavar='URL')
	parser.add_argument(dest='exp', nargs='?', help="""Get data from elements that match CSS SELECTOR""", metavar='SELECTOR')
	parser.add_argument('-d', '--data-attrib', dest='attrib', help="""Use ATTRIBUTE as source of data from elements""", metavar='ATTRIBUTE')
	parser.add_argument('-p', '--proxy', dest='proxy', help="""Download over Proxy URI (e.g. socks5://localhost:9050)""", metavar='URI')
	parser.add_argument('-n', '--next', dest='next_exp', help="""Determine "next" URL by CSS SELECTOR""", metavar='SELECTOR')
	parser.add_argument('-a', '--next-attrib', dest='next_attrib', help="""Use ATTRIBUTE to determine "next" URL from element""", metavar='ATTRIBUTE')
	parser.add_argument('-t', '--title', dest='title_exp', help="""Grab Title from CSS SELECTOR""", metavar='SELECTOR')
	parser.add_argument('-l', '--input-list', dest='input_list', action='store_true', help="""Read input as list of URLs, rather than a single URL or page""")
	parser.add_argument('-c', '--cookiefile', dest='cookiefile', help="""Use FILE for Cookies (read/write)""")
	parser.add_argument('-u', '--user-agent', dest='user_agent', help="""Set User Agent to STRING""", metavar='STRING')
	parser.add_argument('-o', '--output', dest='outfile', default=None, help="""Output to FILE instead of STDOUT""", metavar='FILE')
	parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help="""Output more messages""")
	args = parser.parse_args()

	try:
		req, result = __run((lambda: (sys.stdout.buffer if args.outfile is None else open(args.outfile, 'wb')))
			, (None if args.url == '-' else args.url), args.input_list
			, args.proxy, args.user_agent, args.exp, args.attrib
			, args.next_exp, args.next_attrib, args.title_exp, None
			, args.cookiefile, args.verbose)
	except KeyboardInterrupt:
		req = None
		result = False

	if not sys.flags.interactive:
		sys.exit(0 if result else 1)
	return req

if __name__ == '__main__':
	req = __main()

